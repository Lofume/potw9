<!--- The following README.md sample file was adapted from https://gist.github.com/PurpleBooth/109311bb0361f32d87a2#file-readme-template-md by Gabriella Mosquera for academic use ---> 
<!--- You may delete any comments in this sample README.md file. If needing to use as a .txt file then simply delete all comments, edit as needed, and save as a README.txt file --->

# POTW 9

* Creating a Yatzy game using the rules of Yahtzee

* *Date Created*: 3 December 2022
* *Last Modification Date*: 3 December 2022
* *Lab URL*: <>
* *Git URL*: <https://git.cs.dal.ca/ofume/b00738568_web_centric_computing_csci3172.git>

## Authors
* [Lynda Ofume](Ly863136@dal.ca) - *Developer/Designer*


## Getting Started

- To view the assignment please refer to the Lab URL in order to view code. You must open the JS Console in the browser to view.
- In order to view source code, please view the Git URL to view.


## Sources Used

Used to understand the concept of merging props and conceptualizing the instance of objects, revised to fit the mold of POTW9, 
    https://github.com/Lukazovic/yahtzee-react-app, Accessed December 3, 2022

## Acknowledgments

Lukazovic, https://github.com/Lukazovic/yahtzee-react-app, Accessed December 3, 2022 